#ifndef MUMBLE_H
#define MUMBLE_H

#include <stdint.h>
#include <sys/time.h>

#include "ssl_connection.h"

typedef void mumble_version_callback_t(uint16_t major, uint8_t minor, uint8_t patch, char* release, char* os, char* os_version, void* user_data);
typedef void mumble_audio_callback_t(uint8_t* audio_data, uint32_t audio_data_size, void* user_data);
typedef void mumble_serversync_callback_t(char* welcome_text, int32_t session, int32_t max_bandwidth, int64_t permissions, void* user_data);
typedef void mumble_channelremove_callback_t(uint32_t channel_id, void* user_data);
typedef void mumble_channelstate_callback_t(char* name, int32_t channel_id, int32_t parent, char* description, uint32_t* links, uint32_t n_links, uint32_t* links_add, uint32_t n_links_add, uint32_t* links_remove, uint32_t n_links_remove, int32_t temporary, int32_t position, void* user_data);
typedef void mumble_userremove_callback_t(uint32_t session, int32_t actor, char* reason, int32_t ban, void* user_data);
typedef void mumble_userstate_callback_t(int32_t session, int32_t actor, char* name, int32_t user_id, int32_t channel_id, int32_t mute, int32_t deaf, int32_t suppress, int32_t self_mute, int32_t self_deaf, char* comment, int32_t priority_speaker, int32_t recording, void* user_data);
typedef void mumble_banlist_callback_t(uint8_t* ip_data, uint32_t ip_data_size, uint32_t mask, char* name, char* hash, char* reason, char* start, int32_t duration, void* user_data);
typedef void mumble_textmessage_callback_t(uint32_t actor, uint32_t n_session, uint32_t* session, uint32_t n_channel_id, uint32_t* channel_id, uint32_t n_tree_id, uint32_t* tree_id, char* message, void* user_data);
typedef void mumble_permissiondenied_callback_t(int32_t permission, int32_t channel_id, int32_t session, char* reason, int32_t deny_type, char* name, void* user_data);
typedef void mumble_acl_callback_t(void* user_data);
typedef void mumble_queryusers_callback_t(uint32_t n_ids, uint32_t* ids, uint32_t n_names, char** names, void* user_data);
typedef void mumble_cryptsetup_callback_t(uint32_t key_size, uint8_t* key, uint32_t client_nonce_size, uint8_t* client_nonce, uint32_t server_nonce_size, uint8_t* server_nonce, void* user_data);
typedef void mumble_contextactionmodify_callback_t(char* action, char* text, uint32_t m_context, uint32_t operation, void* user_data);
typedef void mumble_contextaction_callback_t(int32_t session, int32_t channel_id, char* action, void* user_data);
typedef void mumble_userlist_callback_t(uint32_t user_id, char* name, char* last_seen, int32_t last_channel, void* user_data);
typedef void mumble_voicetarget_callback_t(void* user_data);
typedef void mumble_permissionquery_callback_t(int32_t channel_id, uint32_t permissions, int32_t flush, void* user_data);
typedef void mumble_codecversion_callback_t(int32_t alpha, int32_t beta, uint32_t prefer_alpha, int32_t opus, void* user_data);
typedef void mumble_userstats_callback_t(void* user_data);
typedef void mumble_requestblob_callback_t(void* user_data);
typedef void mumble_serverconfig_callback_t(uint32_t max_bandwidth, char* welcome_text, uint32_t allow_html, uint32_t message_length, uint32_t image_message_length, void* user_data);
typedef void mumble_suggestconfig_callback_t(uint32_t version, uint32_t positional, uint32_t push_to_talk, void* user_data);

struct mumble_config
{
  // The size of this structure in bytes
  uint32_t size;
  // The host to connect to (ip or domain name)
  char* host;
  // The port to connect to (default is 64738)
  char* port;
  // The authentication password for the server
  char* server_password;
  // The username to use when joining the server
  char* username;
  // The user cert to presetn
  char* user_cert_filename;
  // The private key for the user cert
  char* user_privkey_filename;

  // ***** CALLBACKS *****

  // The SSL verification function if the server cert does not validate with the system certs
  x509_verify_function* ssl_verification_callback;

  mumble_version_callback_t*             version_callback;
  mumble_audio_callback_t*               audio_callback;
  mumble_serversync_callback_t*          serversync_callback;
  mumble_channelremove_callback_t*       channelremove_callback;
  mumble_channelstate_callback_t*        channelstate_callback;
  mumble_userremove_callback_t*          userremove_callback;
  mumble_userstate_callback_t*           userstate_callback;
  mumble_banlist_callback_t*             banlist_callback;
  mumble_textmessage_callback_t*         textmessage_callback;
  mumble_permissiondenied_callback_t*    permissiondenied_callback;
  mumble_acl_callback_t*                 acl_callback;
  mumble_queryusers_callback_t*          queryusers_callback;
  mumble_cryptsetup_callback_t*          cryptsetup_callback;
  mumble_contextactionmodify_callback_t* contextactionmodify_callback;
  mumble_contextaction_callback_t*       contextaction_callback;
  mumble_userlist_callback_t*            userlist_callback;
  mumble_voicetarget_callback_t*         voicetarget_callback;
  mumble_permissionquery_callback_t*     permissionquery_callback;
  mumble_codecversion_callback_t*        codecversion_callback;
  mumble_userstats_callback_t*           userstats_callback;
  mumble_requestblob_callback_t*         requestblob_callback;
  mumble_serverconfig_callback_t*        serverconfig_callback;
  mumble_suggestconfig_callback_t*       suggestconfig_callback;

  // This pointer is passed to all callbacks. User may pass here the pointer to his data structure.
  void* user_data;
};

/**
 * Connect to the mumble server
 *
 * talloc_context - the talloc context to use (may be NULL)
 * config - the configuration to use
 * returns a context structure, or NULL on error
 */
struct mumble_struct* mumble_connect(void* talloc_context, struct mumble_config* config);

/**
 * Sends Opus audio data to the mumble server.
 */
int mumble_send_audio_data(struct mumble_struct* context, uint32_t sequence_num, uint8_t* audio_data, uint32_t audio_data_size);

/**
 * Sends a ping to the server.
 *
 * This needs to be done at least once every 30 seconds. Currently this is done automatically whenever mumble_tick returns 0.
 *
 */
int mumble_send_server_ping(struct mumble_struct* context);

// Timeval is undefined after the tick
// Timeval should be around 20 seconds as it sends a ping to the server if no messages are received
// Longer than 20 risks having the server disconnect us, shorter than 20 may excessively ping the server
// Returns 0 if timeout occurs, 1 if message was processed, -1 on error
/**
 * Services the mumble connection.
 *
 * Blocks until one of:
 * - A new mumble message arrives and is processed (possibly calling a callback function)
 * - A user supplied fd causes select to return before the specified timeout
 * - The timeout expires
 * - The connection dies
 *
 * Currently this call will also send a server ping if the timeout expires or a user supplied fd causes select to return early.
 *
 * The timeout value should be around 20 seconds. >20 may cause mumble server disconnection.
 *
 * readfds, writefds and exceptfds may be NULL if there are no user supplied fd's that need to be select()ed upon.
 *
 * max_fd should be the value of the highest file descriptor in any fd_set. It should be set to 0 if no fd_set values are given.
 *
 * If tv is NULL then it will be set to 20 seconds
 */
int mumble_tick_full(struct mumble_struct* context, struct timeval* tv, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, int max_fd);

/**
 * Equivalent to mumble_tick_full(context, NULL, NULL, NULL, NULL, 0);
 */
int mumble_tick(struct mumble_struct* context);

/**
 * Closes the connection and frees all resources.
 */
void mumble_close(struct mumble_struct* context);

/**
 * Encodes the integer into variable length integer format.
 * Returns the size of the encoded data.
 */
uint32_t mumble_make_varint(int64_t val, uint8_t* buffer);

/**
 * Parses the variable length integer format into regular integer.
 * Returns the size of the encoded data.
 */
uint32_t mumble_parse_variant(int64_t *val, uint8_t *buffer);

#endif

