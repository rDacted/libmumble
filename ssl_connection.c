#define _POSIX_SOURCE

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include <talloc.h>

#ifdef OPT_TLS_OPENSSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>
#elif OPT_TLS_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#elif OPT_TLS_NONE
// "None" has no dependencies :)
#else
#pragma GCC error "NEED TO SPECIFY -DOPT_TLS_OPENSSL, -DOPT_TLS_GNUTLS or -DOPT_TLS_NONE"
#endif

#include "ssl_connection.h"

struct ssl_connection_struct
{
  int socket_fd;
  char* host;
  char* port;
#ifdef OPT_TLS_OPENSSL
  SSL_CTX* ssl_ctx;
  SSL* ssl;
#elif OPT_TLS_GNUTLS
  gnutls_certificate_credentials_t gnutls_cred;
  gnutls_session_t session;
  x509_verify_function* ssl_verification_function;
#endif
};

/**
 * Connects a TCP socket to the provided hostname and port.
 *
 * IPv6 compatible!
 *
 * It will try all the results for a given hostname until one succeeds.
 *
 * host - The hostname to connect to
 * port - The port number to connect to
 * returns - -1 if the host could not be resolved
 *           -2 if the socket could not be connected
 *           Any other value indicates success
 *
 */
static int tcp_connection(char* host, char* service)
{
  struct addrinfo hints;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;     // Allow IPv4 or IPv6
  hints.ai_socktype = SOCK_STREAM; // TCP socket
  hints.ai_flags = 0;
  hints.ai_protocol = 0;           // Any protocol

  struct addrinfo *result;
  if (getaddrinfo(host, service, &hints, &result) != 0)
  {
    printf("Failed to resolve %s\n", host);
    return -1;
  }

  // getaddrinfo() returns a list of address structures.
  // Try each address until we successfully connect(2).
  // If socket(2) (or connect(2)) fails, we (close the socket
  // and) try the next address.
  int sock = -1;
  for (struct addrinfo* rp = result; rp != NULL; rp = rp->ai_next)
  {
    sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (sock == -1)
    {
      continue;
    }

    if (connect(sock, rp->ai_addr, rp->ai_addrlen) != -1)
    {
      // Win!
      break;
    }

    close(sock);
  }

  freeaddrinfo(result);

  if (sock == -1) {
    printf("Could not connect to %s\n", host);
    return -2;
  }

  return sock;
}

#ifdef OPT_TLS_OPENSSL

// This is an ugly global variable as the ssl cert verification callback does not
// allow arbitrary args to be passed
static x509_verify_function* g_ssl_verification_function = NULL;

/**
 * A callback function for openssl to use when attempting to validate certificates.
 *
 * Refer to openssl documentation for official usage.
 *
 * High level summary:
 * preverify_ok - 0 if vertificate has failed openssl verification. 1 if it verfies
 * store_ctx - the ceritificate being verified
 * returns - 0 if the certificate fails verification, 1 if it succeeds
 */
static int ssl_cert_verification(int preverify_ok, X509_STORE_CTX* store_ctx)
{
  // TODO openssl does not check the CN of the peer cert and compare it against
  // the hostname we tried to connect to. More details here
  // http://wiki.openssl.org/index.php/Hostname_validation
  // For the time being, leave this to the g_ssl_verification_function
  // until OpenSSL 1.0.2 is more popular

  // Preverify means that the cert is valid according to openssl
  if (preverify_ok == 1)
  {
    printf("Warning. When compiled with OpenSSL the hostname is not validated against the server cert\n");
    printf("Therefore certificate validation is being left to the g_ssl_verification_function\n");
//    return 1;
  }

  if (g_ssl_verification_function != NULL)
  {
    uint8_t* data_buffer = NULL;
    uint32_t data_buffer_size = 0;

    STACK_OF(X509)* sk = X509_STORE_CTX_get_chain(store_ctx);
    if (sk != NULL)
    {
      for (int32_t i = 0; i < sk_X509_num(sk); i++)
      {   
        X509* cert = sk_X509_value(sk, i); 

        BIO* b = BIO_new(BIO_s_mem());
        PEM_write_bio_X509(b, cert);
        BUF_MEM* bptr;
        BIO_get_mem_ptr(b, &bptr);
        size_t size = bptr->length;
        const char* data = bptr->data;
 
        data_buffer = realloc(data_buffer, data_buffer_size + size + 1); 
        memcpy(data_buffer + data_buffer_size, data, size);
        data_buffer_size += size;
        data_buffer[data_buffer_size] = '\0';
  
        BIO_free(b);
      }
    }

    int ret = g_ssl_verification_function(data_buffer, data_buffer_size);
    free(data_buffer);
    return ret;
  }

  printf("Failed to verify ssl cert. Details:\n");


  STACK_OF(X509)* chain = X509_STORE_CTX_get_chain(store_ctx);
  for (int i = 0; i < sk_X509_num(chain); i++)
  {
    X509* cert = sk_X509_value(chain, i);

    BIO* mem = BIO_new(BIO_s_mem());
    X509_print_ex(mem, cert, 0, 0);
    BUF_MEM* bptr;
    BIO_get_mem_ptr(mem, &bptr);
    printf("%s\n", bptr->data);
    BIO_free(mem);

//    printf("Name: %s\n", cert->name);
//    printf("Serial: %02X", cert->cert_info->serialNumber->data[0]);
//    for (int j = 1; j < cert->cert_info->serialNumber->length; j++)
//    {
//      printf(":%02x", cert->cert_info->serialNumber->data[j]);
//    }
//    printf("\n");

    // The fingerprint is not part of the printout above
    const EVP_MD* digest = EVP_get_digestbyname("sha1");
    if (digest != NULL)
    {
      unsigned char md[EVP_MAX_MD_SIZE];
      unsigned int n;
      X509_digest(cert, digest, md, &n);
      printf("    Fingerprint: ");
      for (int j = 0; j < 19; j++)
      {
        printf("%02x:", md[j]);
      }
      printf("%02x\n\n", md[19]);
    }
  }

  return 0;
}
#elif OPT_TLS_GNUTLS
static int ssl_cert_verification(gnutls_session_t session)
{
  struct ssl_connection_struct* context = gnutls_session_get_ptr(session);

//  printf("GnuTLS certificate verification callback called\n");

  unsigned int status = 0;
  int ret = gnutls_certificate_verify_peers3(session, context->host, &status);
  if (ret < 0)
  {
    // This is a serious error. It suggests the TLS connection is not proper
    printf("Error validating peer cert!\n");
    return GNUTLS_E_CERTIFICATE_ERROR;
  }

  if (status == 0)
  {
    // The certificate is fine! No need to call the callback
    return 0;
  }

  if (context->ssl_verification_function != NULL)
  {
    // Before calling the callback convert the peer certs to PEM format
    uint8_t* pem_data = NULL;
    uint32_t pem_data_size = 0;
  
    unsigned int cert_list_size = 0;
    const gnutls_datum_t* cert_list = gnutls_certificate_get_peers(session, &cert_list_size);
    if (cert_list == NULL)
    {
      printf("Peer has no certs?\n");
      return GNUTLS_E_CERTIFICATE_ERROR;
    }
  
    for (int i = 0; i < cert_list_size; i++)
    {
      const gnutls_datum_t* cert = &cert_list[i];
  
      size_t buffer_size = 1000 + (cert->size * 3);
      char* buffer = malloc(buffer_size);
      gnutls_pem_base64_encode("CERTIFICATE", cert, buffer, &buffer_size);
  
      pem_data = realloc(pem_data, pem_data_size + buffer_size + 1);
  
      memcpy(pem_data + pem_data_size, buffer, buffer_size);
      pem_data_size = pem_data_size + buffer_size;
      pem_data[pem_data_size] = '\0';
      free(buffer);
    }

    // Call the callback and return what they want
    int ret = context->ssl_verification_function(pem_data, pem_data_size);
    free(pem_data);

    if (ret == 1)
    {
      // The ssl_verification_function returns 1 on success, but we need to return 0
      return 0;
    }
    return GNUTLS_E_CERTIFICATE_ERROR;
  }

  printf("Failed to verify ssl cert. Details:\n");
  unsigned int cert_list_size = 0;
  const gnutls_datum_t* cert_list = gnutls_certificate_get_peers(session, &cert_list_size);
  printf("GnuTLS certificate validation status: ");
  if (status & GNUTLS_CERT_INVALID)
  {
    printf("GNUTLS_CERT_INVALID");
    status ^= GNUTLS_CERT_INVALID;
    if (status != 0)
    {
      printf("|");
    }
  }
  if (status & GNUTLS_CERT_SIGNER_NOT_FOUND)
  {
    printf("GNUTLS_CERT_SIGNER_NOT_FOUND");
    status ^= GNUTLS_CERT_SIGNER_NOT_FOUND;
    if (status != 0)
    {
      printf("|");
    }
  }
  if (status & GNUTLS_CERT_UNEXPECTED_OWNER)
  {
    printf("GNUTLS_CERT_UNEXPECTED_OWNER");
    status ^= GNUTLS_CERT_UNEXPECTED_OWNER;
    if (status != 0)
    {
      printf("|");
    }
  }

  if (status != 0)
  {
    printf("0x%x", status);
  }
  printf("\n");

  if (cert_list_size > 0)
  {
    gnutls_x509_crt_t cert;
    gnutls_x509_crt_init(&cert);
    gnutls_x509_crt_import(cert, &cert_list[0], GNUTLS_X509_FMT_DER);
    printf("Certificate info:\n");

    /* This is the preferred way of printing short information about
     * a certificate. */

    gnutls_datum_t cinfo;

    int ret = gnutls_x509_crt_print(cert, GNUTLS_CRT_PRINT_ONELINE, &cinfo);
    if (ret == 0)
    {
      printf("\t%s\n", cinfo.data);
      gnutls_free(cinfo.data);
    }
    gnutls_x509_crt_deinit(cert);
  }

  return 0;
}

#endif

struct ssl_connection_struct* ssl_connection_connect(void* talloc_context, char* host, char* port, x509_verify_function* callback)
{
  struct ssl_connection_struct* context = (struct ssl_connection_struct*)talloc(talloc_context, struct ssl_connection_struct);
  memset(context, 0, sizeof(struct ssl_connection_struct));
  context->socket_fd = -1;

  if (host == NULL)
  {
    printf("Host cannot be null\n");
    ssl_connection_disconnect(context);
    return NULL;
  }
  context->host = talloc_strdup(context, host);

  if (port == NULL)
  {
    printf("Port cannot be null\n");
    ssl_connection_disconnect(context);
    return NULL;
  }
  context->port = talloc_strdup(context, port);

  context->socket_fd = tcp_connection(host, port);
  if (context->socket_fd < 0)
  {
    printf("Could not connect TCP socket\n");
    ssl_connection_disconnect(context);
    return NULL;
  }

#ifdef OPT_TLS_OPENSSL
  g_ssl_verification_function = callback;

  SSL_library_init();
  SSL_load_error_strings();

  context->ssl_ctx = SSL_CTX_new(SSLv23_client_method());
  if (context->ssl_ctx == NULL)
  {
    printf("Could not create the SSL Context\n");
    ssl_connection_disconnect(context);
    return NULL;
  }

  // Don't support SSLv2 and SSLv3
  SSL_CTX_set_options(context->ssl_ctx, SSL_OP_NO_SSLv2|SSL_OP_NO_SSLv3);

  // Need to verify the server cert
  SSL_CTX_set_verify(context->ssl_ctx, SSL_VERIFY_NONE|SSL_VERIFY_PEER, ssl_cert_verification);

  context->ssl = SSL_new(context->ssl_ctx);
  if (context->ssl == NULL)
  {
    printf("Could not create the SSL* object\n");
    ssl_connection_disconnect(context);
    return NULL;
  }

  
  if (SSL_set_fd(context->ssl, context->socket_fd) == 0)
  {
    printf("Could not set ssl fd\n");
    ssl_connection_disconnect(context);
    return NULL;
  }

  if (SSL_connect(context->ssl) != 1)
  {
    printf("Could not connect to remote endpoint\n");
    ssl_connection_disconnect(context);
    return NULL;
  }
#elif OPT_TLS_GNUTLS
  context->ssl_verification_function = callback;

  gnutls_global_init();

  const char GNUTLS_MIN_VERSION[] = "3.1.4";
  if (gnutls_check_version(GNUTLS_MIN_VERSION) == NULL)
  {
    printf("I require GnuTLS version %s or later\n", GNUTLS_MIN_VERSION);
    ssl_connection_disconnect(context);
    return NULL;
  }

  gnutls_certificate_allocate_credentials(&context->gnutls_cred);
  gnutls_certificate_set_verify_function(context->gnutls_cred, ssl_cert_verification);

  gnutls_init(&context->session, GNUTLS_CLIENT);

  // Save a copy of the context for later retrieval
  gnutls_session_set_ptr(context->session, context);

  gnutls_server_name_set(context->session, GNUTLS_NAME_DNS, host, strlen(host));

  gnutls_set_default_priority(context->session);

  gnutls_credentials_set(context->session, GNUTLS_CRD_CERTIFICATE, context->gnutls_cred);

  gnutls_transport_set_int(context->session, context->socket_fd);
  gnutls_handshake_set_timeout(context->session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

  int ret = 0;
  do
  {
    ret = gnutls_handshake(context->session);
  } while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

  if (ret < 0)
  {
    printf("Failed to establish TLS connection\n");
    ssl_connection_disconnect(context);
    return NULL;
  }
#endif

  return context;
}

int ssl_connection_has_data_available(struct ssl_connection_struct* context)
{
#ifdef OPT_TLS_OPENSSL
  if (SSL_pending(context->ssl) > 0)
  {
    return 1;
  }
#elif OPT_TLS_GNUTLS
  if (gnutls_record_check_pending(context->session) != 0)
  {
    return 1;
  }
#endif

  // The TLS library may not correctly report if it hasn't been able to
  // drain the socket of its bytes
  uint8_t buf[1];
  if (recv(context->socket_fd, buf, sizeof(buf), MSG_PEEK|MSG_DONTWAIT) == sizeof(buf))
  {
    return 1;
  }

  // No data pending
  return 0;
}

int ssl_connection_wait_for_data_available(struct ssl_connection_struct* context, struct timeval* tv, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, int max_fd)
{
  // Check whether there is already data available
  if (ssl_connection_has_data_available(context) != 0)
  {
    return 1;
  }

  // Select on the socket fd
  fd_set readfds_default;
  fd_set writefds_default;
  fd_set exceptfds_default;

  if (readfds == NULL)
  {
    FD_ZERO(&readfds_default);
    readfds = &readfds_default;
  }
  if (writefds == NULL)
  {
    FD_ZERO(&writefds_default);
    writefds = &writefds_default;
  }
  if (exceptfds == NULL)
  {
    FD_ZERO(&exceptfds_default);
    exceptfds = &exceptfds_default;
  }

  FD_SET(context->socket_fd, readfds);
  FD_SET(context->socket_fd, exceptfds);

  if (max_fd < context->socket_fd)
  {
    max_fd = context->socket_fd;
  }

  int select_value = select(max_fd + 1, readfds, writefds, exceptfds, tv);
  if (select_value < 0)
  {
    // Error
    return -1;
  }
  if (select_value == 0)
  {
    // Idle
    return 0;
  }
  
  if (FD_ISSET(context->socket_fd, exceptfds))
  {
    // Error?
    return -1;
  }

  if (FD_ISSET(context->socket_fd, readfds))
  {
    return 1;
  }

  // Otherwise one of the user provided fd's has triggered
  return 0;
}

int ssl_connection_read(struct ssl_connection_struct* context, void* buffer, uint32_t amount_to_read)
{
#ifdef OPT_TLS_OPENSSL
  return SSL_read(context->ssl, buffer, amount_to_read);
#elif OPT_TLS_GNUTLS
  return gnutls_record_recv(context->session, buffer, amount_to_read);
#elif OPT_TLS_NONE
  return recv(context->socket_fd, buffer, amount_to_read, MSG_WAITALL);
#endif
}

int ssl_connection_write(struct ssl_connection_struct* context, void* buffer, uint32_t amount_to_write)
{
#ifdef OPT_TLS_OPENSSL
  return SSL_write(context->ssl, buffer, amount_to_write);
#elif OPT_TLS_GNUTLS
  return gnutls_record_send(context->session, buffer, amount_to_write);
#elif OPT_TLS_NONE
  return send(context->socket_fd, buffer, amount_to_write, 0);
#endif
}

void ssl_connection_disconnect(struct ssl_connection_struct* context)
{
#ifdef OPT_TLS_OPENSSL
  if (context->ssl != NULL)
  {
    SSL_free(context->ssl);
    context->ssl = NULL;
  }
  if (context->ssl_ctx != NULL)
  {
    SSL_CTX_free(context->ssl_ctx);
    context->ssl_ctx = NULL;
  }
#elif OPT_TLS_GNUTLS
  if (context->session != NULL)
  {
    gnutls_deinit(context->session);
    context->session = NULL;
  }

  if (context->gnutls_cred != NULL)
  {
    gnutls_certificate_free_credentials(context->gnutls_cred);
    context->gnutls_cred = NULL;
  }

  gnutls_global_deinit();

#endif

  if (context->socket_fd != -1)
  {
    close(context->socket_fd);
    context->socket_fd = -1;
  }
  talloc_free(context);
}

