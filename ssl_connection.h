#ifndef SSL_CONNECTION_H
#define SSL_CONNECTION_H

#include <stdint.h>

typedef int x509_verify_function(uint8_t* x509_pem_certificates, uint32_t size);

/**
 * Creates an ssl_connection object connected to the ipv4 IP and port.
 *
 * host - The hostname to connect to
 * port - The port number to connec to
 * talloc_context - The talloc context. May be NULL.
 * verify_cert - A callback function used to validate the server cert
 * returns - The object or NULL on error
 */
struct ssl_connection_struct* ssl_connection_connect(void* talloc_context, char* host, char* port, x509_verify_function* callback);

/**
 * Performs a non-blocking check to see if there is data available to be read by SSL_read
 *
 * context - The ssl connection to query
 * returns - 0 if no data is available. 1 if data is available
 */
int ssl_connection_has_data_available(struct ssl_connection_struct* context);

/**
 * Blocks for the duration specified in the timeval until data is available or the timeout occurs.
 *
 * If tv is NULL this function may block forever. A tv value of {0,0} will return immediately.
 *
 * context - The ssl connection to query
 * tv - the time to wait
 * returns - -1 on error. 0 if there is no data available. 1 if data is available
 */
int ssl_connection_wait_for_data_available(struct ssl_connection_struct* context, struct timeval* tv, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, int max_fd);

int ssl_connection_read(struct ssl_connection_struct* context, void* buffer, uint32_t amount_to_read);

int ssl_connection_write(struct ssl_connection_struct* context, void* buffer, uint32_t amount_to_write);

/**
 * Destroys an ssl_connection object.
 *
 * context - the object to destroy
 */
void ssl_connection_disconnect(struct ssl_connection_struct* context);

#endif

